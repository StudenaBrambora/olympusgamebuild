# Olympus

*The mountains shudder, ocean waves grow restless and the dark red sky looms over the
ancient Greece. The gates of mythical Olympus, the home of Gods, are open once more.
Gather your things and set out towards your destiny. Make haste though, for you are not
alone. Other mortals will surely seek to claim the riches of Olympus for themselves and they
could make for either powerful allies, or, perhaps, unyielding foes. From high atop his
mountain, Zeus, the highest of gods, watches, but while the mortals race for the sky throne,
his gaze is fixed only upon one of them…
Could it be you?*

Olympus is a digital board game for 4 to 7 players, set in times of ancient Greece and mythology.

While one of the players takes the role of Zeus, others control their mortal characters and
move them around the hexagonal game board. In the middle lies their goal, the Olympus.
The goal of every mortal is to get to the Olympus first, but the journey up there will be
dangerous. First, they must gather enough equipment and followers to traverse the
mountain range safely. Equipment can be either found in ancient ruins, bought at cities and
settlements, or perhaps even usurped from other mortals.
Zeus himself also has a goal – to help his chosen mortal (picked at the beginning, kept secret)
fulfil his goal. By blessing mortals and playing global event cards, he can indirectly influence
the game in their favour. But he must do so very carefully. Should the mortals discover the
chosen one, they could team up to remove them from the game.

## Features
- Hexagonal game board with different terrains and tiles.
- Over 250 unique cards with equipment, followers, encounters and events themed around ancient Greek mythology.
- Player to player dice combat and encounters.
- Social deduction elements.

## Rule summary
Before the beginning of the game, one player is selected to figure as Zeus, while all other players will play as Mortals and place their figure on one of the starting points on a hexagon board. In the middle of the board is the Mount Olympus, a goal for all mortals to reach.

At the beginning of the game, Zeus will **secretly** and **randomly** pick one Mortal, who will be their Chosen.
Players then take turns: Mortals move about the hexagon map completing encounters, collecting equipment, artifacts and fame, even battling other Mortals, should they choose. Zeus does not partake directly on the map, but has the ability to influence the game board, the encounters and help or harm the Mortals.

The game ends when either: 
1. One of the Mortals succesfully reaches the top of Mount Olympus. Beware, that only the worthy may reach the peak; one must either gather enough artifacts, amass enough fame to their name, or stand Zeus's trial and demonstrate their combat prowess. The first Mortal to reach the top is victorious, and if the Mortal is also revealed to be Zeus's Chosen, then also Zeus is victorious.
2. Zeus's Chosen is killed by another. In such case, all other Mortals are victorious.

The complete set of rules in czech is [included](https://gitlab.com/StudenaBrambora/olympusgamebuild/-/blob/main/Olympus%20-%20p%C5%99%C3%ADru%C4%8Dka.pdf).

## Preview
![alt text](https://gitlab.com/StudenaBrambora/olympusgamebuild/-/raw/main/Preview%20screenshots/Olympus1.png "Olympus")
![alt text](https://gitlab.com/StudenaBrambora/olympusgamebuild/-/raw/main/Preview%20screenshots/Olympus2.png "Olympus")
![alt text](https://gitlab.com/StudenaBrambora/olympusgamebuild/-/raw/main/Preview%20screenshots/Olympus3.png "Olympus")
![alt text](https://gitlab.com/StudenaBrambora/olympusgamebuild/-/raw/main/Preview%20screenshots/Olympus4.png "Olympus")
![alt text](https://gitlab.com/StudenaBrambora/olympusgamebuild/-/raw/main/Preview%20screenshots/Olympus5.png "Olympus")

## Note
This project serves as a tool for quick iterative prototyping and playtesting of the Olympus board game.
Source code is available on [GitLab](https://gitlab.com/StudenaBrambora/olympus "Olympus source code").

Olympus has been created as a part of Game Design course at FI MUNI by Vladimír Žbánek.
Special thanks goes to:
- Kateřina Klímková (game design, sprites and art)
- Joseph Adam Saunders (card ideas, translation correction, playtesting)
- Lukáš Kejř (game mechanics ideas, playtesting)
- Adéla Budínská (playtesting)
- Dominik Juříček (playtesting)
- Martin Kožnárek (playtesting)
- Petr Novotný (playtesting)
- Milan Smiešný (playtesting)
